package com.survey.survey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Survey {
    private @Id @GeneratedValue Long id;
    @Column(unique=true)
    private String email;
    private int option;

    Survey(String email, int option){
        super();
        this.email = email;
        this.option = option;
    }

    Survey(){

    }

    public Long getId(){
        return this.id;
    }
    public String getEmail(){
        return this.email;
    }

    public int getOption(){
        return this.option;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public void setOption(int option){
        this.option = option;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
