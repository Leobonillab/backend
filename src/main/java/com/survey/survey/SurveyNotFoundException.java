package com.survey.survey;


class SurveyNotFoundException extends RuntimeException {
    SurveyNotFoundException(Long id) {
        super("Could not find survey " + id);
    }
}