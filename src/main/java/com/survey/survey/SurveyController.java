package com.survey.survey;

import java.util.List;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
class SurveyController {

    private final SurveyRepository repository;

    SurveyController(SurveyRepository repository) {
        this.repository = repository;
    }

    // Aggregate root

    @GetMapping("/surveys")
    List<Survey> all() {
        return repository.findAll();
    }

    @PostMapping("/survey/create")
    Survey newSurvey(@RequestBody Survey newSurvey) {
        return repository.save(newSurvey);
    }

    // Single item

    @GetMapping("/survey/{id}")
    Survey one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new SurveyNotFoundException(id));
    }

    @GetMapping("/email/{email}")
    Survey one(@PathVariable String email) {

        return repository.findByEmail(email) ;
    }

    @GetMapping("/count/")
    String one() {
       String json =  "{"
                + "\"1\": \" "+repository.countByOption(1)+" \","
                + "\"2\": \" "+repository.countByOption(2)+" \","
               + "\"3\": \" "+repository.countByOption(3)+" \","
               + "\"0\": \" "+repository.countByOption(0)+ " \""
                + "}";
       return json;
        }



    @PutMapping("/surveys/{id}")
    Survey replaceSurvey(@RequestBody Survey newSurvey, @PathVariable Long id) {

        return repository.findById(id)
                .map(survey -> {
                    survey.setEmail(newSurvey.getEmail());
                    survey.setOption(newSurvey.getOption());
                    return repository.save(survey);
                })
                .orElseGet(() -> {
                    newSurvey.setId(id);
                    return repository.save(newSurvey);
                });
    }

    @DeleteMapping("/survey/{id}")
    void deleteSurvey(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
