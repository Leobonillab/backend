package com.survey.survey;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

interface SurveyRepository extends JpaRepository<Survey, Long> {
Survey findByEmail(String email);
@Query("select count(e) from Survey e where e.option = ?1")
    long countByOption(int option);
}